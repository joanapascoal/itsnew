from django.apps import AppConfig


class RemoteApiConfig(AppConfig):
    name = 'remote_api'
